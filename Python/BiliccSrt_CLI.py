import argparse, BiliccSrt_Down
parser = argparse.ArgumentParser()
parser.add_argument("bv_num", help="视频BV号")
parser.add_argument("-e", "--encoding", choices=["utf-8", "utf-16"], default="utf-8", help="编码")
args = parser.parse_args()
BiliccSrt_Down.bianma = args.encoding
BiliccSrt_Down.downAll(args.bv_num)
